module.exports = function (grunt) {

 grunt.loadNpmTasks('grunt-contrib-copy');
 grunt.loadNpmTasks('grunt-contrib-jshint');
 grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-sync');

 grunt.initConfig({


	sync: {

         client: {

			 files: [{
				 cwd: 'shared/',
				 src: ['**'],
				 dest: '../shared/',
			 }],
			 pretend: false, // Don't do any IO. Before you run the task with `updateAndDelete` PLEASE MAKE SURE it doesn't remove too much.
			 verbose: true // Display log messages when copying files
           },

		repo: {
			files: [{
				cwd: '../shared/',
				src: ['**'],
				dest: 'shared/',
			}],
			pretend: false, // Don't do any IO. Before you run the task with `updateAndDelete` PLEASE MAKE SURE it doesn't remove too much.
			verbose: true // Display log messages when copying files
		}
          

        },

	
 	copy: {
 	      
 	      main: {
 	         files: [
 	         {
 	           expand: true, 
 	           cwd: 'src/',
 	           src: ['**'],
 	           dest: 'dist/web/',
 	           filter: 'isFile'
 	         } 	         
 	         ]
                 	      
 	      },

 	      build: {
 	         files: [
 	         {
 	           expand: true, 
                   cwd: 'src/',
 	           src: ['**'],
 	           dest: 'build/base/',
 	           filter: 'isFile'
 	         } 	         
 	         ]
                 	      
 	      },

 	      shared: {
 	         files: [
 	         {
 	           expand: true, 
                   cwd: 'shared/',
 	           src: ['**'],
 	           dest: 'build/base/js/',
 	           filter: 'isFile'
 	         } 	         
 	         ]
                 	      
 	      },

	  
 	      
 	     premin: {
 	        files: [
 	         {
 	           expand: true, 
 	           cwd: 'build/base',
 	           src: ['**'],
 	           dest: 'build/min/',
 	           filter: 'isFile'
 	         } 	         
 	         ]
 	      },

 	     postmin: {
 	        files: [
 	         {
 	           expand: true, 
 	           cwd: 'build/min',
 	           src: ['**'],
 	           dest: 'dist/',
 	           filter: 'isFile'
 	         } 	         
 	         ]
 	      },
 	      
 	      
 	      
 	     modules: {
 	      	files: [
 	          	  {
 	           expand: true, 
 	           cwd: '../modules/',
 	           src: ['**'],
 	           dest: 'build/base/modules/',
 	           filter: 'isFile'
 	             } 	
 	         
 	         
 	         
 	         ]
 	     
 	     
 	     
 	     
 	     }
 	
 	},
 	
 	
 	jshint: {
 		 
 	    all: ['Gruntfile.js', 'build/base/js/**/*.js', '!build/base/js/lib/**']
 	},
 	
 	
 	uglify: {
 	
 	     main: {
 	        files: [
                   {
                     expand: true,
                     cwd: 'build/min/js/',
                     src: '*.js',
                     dest: 'build/min/js/'                   
   
                   } 	        
 	        
 	        ]
              	     
 	     },
 	 
 	    options: {
 	       mangle:false
 	    }
 	}
 	
 });


grunt.registerTask('syncShared',  ['sync:client', 'sync:repo']);

grunt.registerTask('build', ['copy:build', 'syncShared', 'copy:shared', 'jshint', 'copy:modules', 'minify']);
 

grunt.registerTask('minify', ['copy:premin', 'uglify', 'copy:postmin']);





};
