/**
 * Created by sam on 6/23/15.
 */


var host = "http://lpaasbackend-teachx.rhcloud.com/convertPPT";
//var host = "http://localhost:8080/convert";

window.AudioContext = window.AudioContext || window.webkitAudioContext;
navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;



var app = angular.module('app', [])
    .config( [
        '$compileProvider',
        function( $compileProvider )
        {
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|blob):/);
            // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
        }
    ]).
    controller('mainController', function ($scope, $interval, $timeout, $sce) {


        i18n.init(function (err, t) {
            $scope.t = t;
            $scope.$apply();
        });




        $scope.refresh = function(current, callback){

            $scope.current =  current || $scope.current;

            callback = callback = function () {};

            $scope.slides = [];


            $scope.dotLearn.data.slides.forEach(function(slide){


                var slideLinks = {};


                if(slide.image) slideLinks.image = URL.createObjectURL($scope.dotLearn.file.item.get(slide.image));
                if(slide.audio) slideLinks.audio = $sce.trustAsResourceUrl(URL.createObjectURL($scope.dotLearn.file.item.get(slide.audio)));

                console.log(slideLinks.audio);


                $scope.slides.push(slideLinks);

            });


            $scope.state = 'pre-record';


            util.safeApply($scope);


            callback();


        };


        $scope.setCurrent = function(index){

            console.log(index);
            $scope.current = index;



            scroll2Center(index);



        };

        var rec;




        $scope.state='pre-record';





        $scope.deleteRecording = function(){

            var slide =  $scope.dotLearn.data.slides[$scope.current];

            $scope.dotLearn.file.item.remove(slide.audio);

            delete slide.audio;


            $scope.refresh(null);


        };


        $scope.recordAudio = function(){



            function audioSetup (callback){
                if(rec) callback();
                else{
                    audioRecorder.requestDevice(function(recorder){
                      rec = recorder;
                        callback();
                    }, {workerPath: 'lib/audio/recorderWorker.js', recordAsMP3:true});
                }
            }



            function prePrecording(callback){

                var cancelled = false;


                $scope.state = 'pre-recording';
                util.safeApply($scope, function(){

                   sw.countdown(3000, function(){

                       if(!cancelled) callback();
                   }, 200);
                });



                $scope.cancelRecording = function(){

                  sw.cancel();
                  cancelled = true;
                    callback("Cancelled");

                };




            }



            function beginRecording(callback){
                $scope.state = 'recording';
                util.safeApply($scope, function(){
                    console.log(rec);
                    rec.record();
                    sw.init();
                    callback();
                });
            }



            async.series([audioSetup, prePrecording, beginRecording], function(err){


                if(err) {
                    $scope.state = 'pre-record';
                    util.safeApply($scope);
                }

            });



        };





        $scope.stopRecording = function(){

            rec.stop();

            sw.stop();
            sw.reset();

            $scope.state = 'processing';
            util.safeApply($scope, function(){


                processAudio(function(){



                    rec.clear();

                    $scope.state = 'recorded';
                    $scope.refresh(null, function(){
                        util.safeApply($scope);
                    });
                });


            });






        };



        function processAudio(callback){


            rec.exportMP3(function(mp3Blob){


                console.log(mp3Blob);


                var slide =  $scope.dotLearn.data.slides[$scope.current];



                var base  = slide.image.split('.')[0];
                var itemName = base + '.mp3';

                console.log(itemName);
                $scope.dotLearn.file.item.set(itemName, mp3Blob);
                slide.audio = itemName;



                callback();

            });






        }


        $scope.recordTime = function(){

            var time = sw.getTime();

            var minutes = Math.floor(time/60/1000);

            if (minutes < 10) minutes = "0" + minutes;

            var seconds = Math.floor((time - minutes *60*1000)/1000);

            if (seconds < 10) seconds = "0" + seconds;

            return minutes + ":" + seconds;


        };

        $scope.countdownTime = function(){

            var time = sw.getTime();

            var minutes = Math.floor(time/60/1000);

            if (minutes < 10) minutes = "0" + minutes;

            var seconds = Math.ceil((time - minutes *60*1000)/1000);


            return seconds;


        };



        var setupAudio = function(callback){


            callback();

        };




        var screenwidth = jQuery(window).width();
        var screenheight = jQuery(window).height();




        setTimeout(function () {


            jQuery("#side-panel").sortable({
                axis: "x",
                containment: "document",
                stop: $scope.reOrder
            });
        }, 2000);

        $scope.reOrderReady = true;


        $scope.reOrder = function(){


            if ($scope.reOrderReady){


                $scope.reOrderReady = false;

                var newOrder = [];

                jQuery.each(jQuery("#side-panel").children(), function(index, value){



                    var id = value.id;
                    var new_index = parseInt(id.split("_")[1]);

                    newOrder.push(new_index);


                });



                var newArray = [];

                newOrder.forEach(function(index){

                    newArray.push($scope.dotLearn.data.slides[index]);
                });


                $scope.dotLearn.data.slides = newArray;

                $scope.reOrderReady = true;

                $scope.refresh(null, function(){

                });

            }


        };



        var sw = new util.stopWatch({interval: $interval, timeout: $timeout});






        var scroll2Center = function(index){


            var pos = index + 1;

            var cutoff = Math.round((screenwidth/200)/2);

            console.log("Cutoff: " + cutoff);
            console.log(pos);

            if(pos < cutoff){


                jQuery("#side-panel").animate({scrollLeft: 0}, 800);

            } else if(pos >($scope.dotLearn.data.slides.length -cutoff)){

                jQuery("#side-panel").animate({scrollLeft: ($scope.dotLearn.data.slides.length -cutoff)*200}, 800);


            } else{
                jQuery("#side-panel").animate({scrollLeft: (pos-cutoff)*200}, 800);

            }





        };

        $scope.current = 0;


        $scope.nextSlide = function(){

            $scope.setCurrent($scope.current+1);

        };

        $scope.previousSlide = function(){

            $scope.setCurrent($scope.current-1);
        };




        $scope.removeSlide = function(index){


            $scope.dotLearn.data.slides.splice(index, 1);

            $scope.refresh();
        };




        $scope.loading = 'false';

        $scope.invalidPDF =false;


        window.a = function (index){

            console.log($scope.getSlideImage(index));
        };

        $scope.wrongFileType = function () {
            alert(t("problem.file", {type: t('presentation')}));
            $scope.invalidPDF =true;
        };


        var getConverted = function (callback) {

            var file = $scope.file;

            $scope.invalidPDF =false;

            $scope.loading = true;

            var formData = new FormData();

            formData.append('file', file);

            var xhr = new XMLHttpRequest();

            xhr.onerror = function (e) {
                console.log(e);
            };


            xhr.responseType = "arraybuffer";


            xhr.addEventListener("progress", function (oEvent) {


                if (oEvent.lengthComputable)
                    console.log("Download progress " + Math.round((oEvent.loaded / oEvent.total) * 100));
            }, false);


            xhr.onload = function (oEvent) {

                console.log(oEvent);

                var blob = new Blob([oEvent.target.response], {type: "application/zip"});

                callback(null, blob);

            };


            xhr.upload.addEventListener("progress", function (e) {
                if (e.lengthComputable) {

                    console.log("Upload progress " + Math.round((e.loaded / e.total) * 100));

                }
            }, false);


            xhr.open('POST', host, true);
            xhr.send(formData);



        };


        var  convertToAudioSlideshow = function(file){

            $scope.file = file;

            async.waterfall([getConverted, extractEntries], function(err){

                $scope.screen ='slideshow';

                $scope.refresh(0, function(){



                });


            });
        };









        $scope.setFile = function (file) {

            //   if(file.type != "application/pdf") $scope.wrongFileType();
            // else convertToAudioSlideshow(file);
            convertToAudioSlideshow(file);

        };


        var extractEntries = function(blob,callback){


            console.log("Extracting Entries");

            zip.createReader(new zip.BlobReader(blob), function(zipReader) {
                // get entries from the zip file
                zipReader.getEntries(function(entries) {
                    // get data from the first file


                    var extractEntry = function (entry, callback) {

                        var filename = entry.filename;

                        console.log("Extracting entry " + filename);

                        entry.getData(new zip.BlobWriter("image/png"), function(data) {
                            // close the reader and calls callback function with uncompressed data as parameter

                            console.log("Extracted entry " + filename);

                            $scope.dotLearn.file.item.set(filename, data);


                            $scope.dotLearn.data.slides.push({
                                image: filename,
                                audio: "",
                                caption: ""
                            });



                            callback(null);
                        });



                    };



                    async.eachSeries(entries, extractEntry, function(err){

                        if(err) return callback(err);
                        else callback(null);

                    });



                });
            }, onerror);








        };

        window.onLoad = function (dotLearn) {


            $scope.dotLearn = dotLearn;

            if(dotLearn.data.slides.length > 0){
                $scope.screen = 'slideshow';
                $scope.refresh(0, function(){

                });


                document.getElementById("loader").style.visibility ="hidden";
                document.getElementById("container").style.visibility ="visible";

            } else{
                $scope.screen = 'init';


                util.safeApply($scope, function(){
                    document.getElementById("loader").style.visibility ="hidden";
                    document.getElementById("container").style.visibility ="visible";

                });
            }


        };



        window.onFinished = function () {

            return JSON.parse(JSON.stringify($scope.dotLearn.data));
        };




    }).directive('videoFile', function(){
        return {
            scope: {
                videoFile: '='
            },
            link: function(scope, el, attrs){
                el.bind('change', function(event){



                    var files = event.target.files;
                    var file = files[0];

                    scope.file = file;

                    scope.$parent.setFile(file);

                    util.safeApply(scope);

                });
            }
        };
    });

