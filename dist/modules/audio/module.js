dotlearn.module('audio', function (moduleManager) {
	
    moduleManager.init({ html:'base.html', css:'audio.css',  scripts:['audio.js']}, function (app) {

    });
    
    moduleManager.onStart({}, function (app) {
    
         				audiojs.events.ready(function() {
							    var as = audiojs.createAll();
							  });
    
    });
    
        moduleManager.onLoad({file:true, blocker: true}, function (app) {
        	
        	console.log('Starting load for audio');
          try {
              var content =  document.getElementById('content');
              content.innerHTML = '';
                  
                  
              var header = document.getElementById('header');
              header.innerHTML = '';
              
              if(typeof app.data.caption !== 'undefined'){
                    header.appendChild(document.createTextNode(app.data.caption));              
              } else{
              	
              	     header.appendChild(document.createTextNode(app.data.location.split('/').pop()));
              }
                    
                    app.blocker.block();

                      app.file.get(app.data.location,function (data) {
                      	
                      	                var img  = document.createElement('img');
                      	                
                      	                img.src = 'play.png';
                      	                
                      	                img.width = 200;
                      	                
                      	                img.style.position = "absolute";
                      	                
                      	                img.style.left = "140px";
                      	                
                      	                img.style.top = "1px";
                      	                
                      	                content.appendChild(img);
                      
                      
                      		              var audio = document.createElement("audio");
										                   audio.className = "viewer";														
																 audio.controls = true;
																 audio.id = "audio";

																 audio.src = window.URL.createObjectURL(data);
																 
																     	                audio.style.position = "absolute";
                      	                
                      	                audio.style.left = "100px";
                      	                
                      	                audio.style.top = "220px";
													           
												             content.appendChild(audio);
												             console.log('audio loaded');
													     
													   app.blocker.unblock();     
                                                  													          
													          
			                                        audio.onended = function(){
			      					                    	  	
			      					  	                            } 	
             
                      
                      });
                      
                   } catch (e) {
                                //Something happened
                                  console.log('Something happened');
                                  console.log(e);    
                   
                   }
    });
    
    
    moduleManager.onNext({}, function (app) {
    
                  var audio = document.getElementById('audio');
                  
                  
            try{      
               
                  if(typeof audio !== 'undefined'){
                  	
                  	 if (typeof audio !== 'null') {
                           audio.pause();
                        }         
                        else {
                             //Audio doesn't exist - why?
                            console.log('Something is up here - audio is null');
                       }    
                  
                  }  else{
                         console.log('Something is up here - audio is undefined');    
                  }   
                  
               }  catch (e) {
                
                        //Some error going on here     
                         console.log('Something is up here ');         
                             
                }  
         
    });
    
        moduleManager.onEdit({template:'filepicker.html', structure: {
	         		   location: '',
	         		   caption: 'Caption',
	         		   source: {
	         		     copyright: '',
	         		     license: 'cc',
	         		     source: 'self'
	         		   }
	         }	
	      }, function (app) {
         
         });
    

});