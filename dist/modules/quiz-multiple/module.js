dotlearn.module('quiz-multiple', function (moduleManager) {
	
    moduleManager.init({}, function (app) {
     
     });
    
    moduleManager.onLoad({user:true}, function (context) {
             
          var user = context.user.get();   

          var content = document.getElementById('content');
          content.innerHTML = '';             
             
              var data = context.data;
                              
             
                  var div = document.createElement('div');
					   var quiz = data;
					   
                  div.className = 'quiz-text';
					  	div.appendChild(document.createTextNode(quiz.prompt));
					  	
					  	   var answers = document.createElement('form');
							answers.name = "quiz";
							var 	inputType = 'radio';         
							
                     quiz.options.forEach(function(option, index){               // for each quiz option, add an input
								
								var row = document.createElement('div');
								row.className = 'quiz-option';			  

							  var input = document.createElement('input');
							  input.type = inputType;
							  input.value = index;
								input.name = 'quiz';
								
								if(typeof user != 'undefined'){

									if(user == index){
								      input.checked = true;
								   }
								}							 								
									 			  
							  row.appendChild(input);                              
							  row.appendChild(document.createTextNode("  "+option.text));                    
								 answers.appendChild(row);
								});		
								
	
							  div.appendChild(answers);
							  
							  document.getElementById('content').appendChild(div);
    
    });
    
    moduleManager.onNext({user:true}, function (app) {
    	
    	                 var responses = document.forms['quiz']['quiz'];	 		
                         
                         for (var i=0; i<responses.length; i++){
											if (responses[i].checked){
														app.user.set(i);
											 }
								 }
              
    
    });
    
    moduleManager.onEnd({frame:true, user:true}, function (app) {
    	
    	    var data = app.data;
    	    var frame = app.frame;
 
 	
		
    	    var user = app.user.get();
    	
    				             var answerbox = document.createElement('div');
					    		 	 answerbox.className = 'answer-box';
					    		 	 
					    		 	 var answertext = document.createElement('div');
					    		 	 answertext.className = 'answer-question';
					    		 	 answertext.appendChild(document.createTextNode(data.prompt));
					    		    answerbox.appendChild(answertext);
				
					    		   var response = user;
					    		   var answer = data.answer;
					 
			
									answertext.style = " font-weight: bold; font-size: 14px; font-family:arial; color:#555; margin:2px;";

					
											var yesno = document.createElement('div');
											yesno.className = 'answer-yesno';	
											
																	
										  yesno.style = "font-weight: bold; font-size: 14px; float:right; font-family:arial; color:#555;";											
											    		
								
					    		
											if(user != data.answer){							
													yesno.appendChild(document.createTextNode('Incorrect'));								
												}
												else {
													yesno.appendChild(document.createTextNode('Correct'));												
												}
					    		  		answerbox.appendChild(yesno);    
					    		 		
					              	if(user != data.answer){		
					    		 				if (typeof  data.options[user] != 'undefined'){
													var userresponse = document.createElement('div');
													userresponse.className = 'answer-text';
													var responsetext = 'Your answer' + ': ';
	                                   
	                                 //   console.log(user);
				                           responsetext = responsetext + data.options[user].text;									
													
													userresponse.appendChild(document.createTextNode(responsetext));		
												
													userresponse.style = "font-weight: bold; margin: 2px; font-size: 14px; font-family:arial;";
													answerbox.appendChild(userresponse);
													
									
												  	var explanation = document.createElement('div');
													 explanation.className = 'answer-text';
													 explanation.appendChild(document.createTextNode(data.options[user].explanation));
													 explanation.style = "margin: 2px; font-size: 14px; font-family:arial;";
													 answerbox.appendChild(explanation);					 		
												}						
														
										}		



													var correctanswer = document.createElement('div');
													correctanswer.className = 'answer-text';
													var correcttext = 'Correct Answer'+ ': ';
													
				
								             correctanswer.style="font-weight: bold; margin: 2px; font-size: 14px; font-family:arial;";
				                         correcttext = correcttext + data.options[data.answer].text;								
														
												 correctanswer.appendChild(document.createTextNode(correcttext));		
												 answerbox.appendChild(correctanswer);
												
												
				                         var explanation = document.createElement('div');
																 explanation.className = 'answer-text';
																 explanation.appendChild(document.createTextNode(data.options[data.answer].explanation));
																 answerbox.appendChild(explanation);	
																 
											    explanation.style = "margin:2px; font-size: 14px; font-family:arial;";	
													    	
    	 
                                 
         
                                  frame.appendChild(answerbox); 

    	
    });
    
    moduleManager.onEdit({template:'quiz.html', structure: {
	             	   answer: 0,
	             	   explanation: 'the cake is a lie',
	             	   prompt: 'Who is john galt?',
	             	   options: [{text: 'Some guy', explanation: 'Who knows why?'}]
	             	}
	      }, function (app) {
         
         });
	
});