/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattachatryya

File: player.js

Description: AngularJS module for handing the lesson player functionality - managing all the core functionality of the lesson player. 
This module interacts heavily with the moduleManager module, trigerring the events of most stages of the module lifecycle.

For more info, check out the info on the lesson Player on the wiki/readme.


=======================================================================================================================================*/


angular.module('player', ['user', 'module', 'debug']).
value('lesson', {}).
value('div', document.getElementById('play-item')).
value('i', 0).
value('blocked', false).
factory('playerHandler', function (lesson, i,  div, courseData, blocked, moduleManager, debug) {
	
	return {
	
	   open: function (inputLesson) {                             // On Opening the Lesson for the first time
	   	lesson = inputLesson;
	   	courseData.init(lesson);
	   	this.setItem(0);
      
   		moduleManager.start();
	   },
	   
	   
	   module: '',                                              // Stores the value of the current module, is the model value exposed to the AngularJS view which determines the currently visible iFrame
	  	                                                         // i.e, if the current item uses the image module, this should be set to image, so that the view will show the image iFrame, and not say the 
	  	                                                         // video iframe or the audio iframe
	   
	   setItem: function (index) {                              // Set Item shows item i of the lesson on the screen. Does this by seting up the parameters for the "onLoad" event of the module cycle
	   	
        
         if(!blocked){	   	
	   	
	   	i = index;
	
 		   var item = lesson.getItem(i);
        
         this.module = item.module;         
         
          debug.log('Setting item to '+ (index+1) + ' which is of type ' + item.module, false);
         
         var params = {
          
            name: item.module,
            data: item.data,
            user: courseData.handler(i),
            loader: {
                 get: lesson.getResource            
            },
            blocker: this.blocker
         
         }; 
         
          moduleManager.load(params);
          
         } else{
              console.log('blocking');         
         }
	   	
	   },
	   
	   getLabel: function () {                                    // return the label of the current item (used by the view - to show the current item's label)
	   	
	   	if (lesson.getItem != 'undefined'){
               return '';	   	
	   	} else if (i == lesson.length){
	            return 'End of Lesson';	
	   	
	   	} else {
	   		
              return lesson.getItem(i).label;   	
	   	}
	   },
	   
	   onNext: function () {
	   
	   
                 var item = lesson.getItem(i);
       		   
	   
	            debug.log('Firing next for item ' + (i+1), false);
          
		         var params = {
		          
		            name: item.module,
		            data: item.data,
		            user: courseData.handler(i),
		            loader: {
		                 get: lesson.getResource            
		            },
		            blocker: this.blocker
		         
		         };           
		          
		 
		          moduleManager.next(params);
	   
	   
	   },
	
		
	          
      next: function () {                                                            // Contains the non-ui logic for Next, including calling the onNext event for the previous item, and either the onLoad 
      																										// for the next item (via set Item) or the lesson end event
      	
       if(!blocked){
      	 var item = lesson.getItem(i);
          var handler = this;      	
          
         debug.log('Firing next for item ' + (i+1), false);
          
         var params = {
          
            name: item.module,
            data: item.data,
            user: courseData.handler(i),
            loader: {
                 get: lesson.getResource            
            },
            blocker: this.blocker
         
         };           
          
 
          moduleManager.next(params);
          
        //  console.log(lesson);
          
          if(i < (lesson.length-1)){
          	
          	   this.setItem(i+1);

          } else if(i == (lesson.length-1)){
   
              i++;
              this.endofLesson();
             
          }
          
          
            
      	
         }

       },
		       
      back: function () {                                                                 //Same as for forwards, but for backwards
      	
      	
      	
              debug.log('Firing back for item ' + (i+1), false);       	
      	
      	   if(i < lesson.length){
      	
      		 var item = lesson.getItem(i);
      		 
      		 if(!blocked){
      	
			         var params = {
			          
			            name: item.module,
			            data: item.data,
			            user: courseData.handler(i),
			            loader: {
			                 get: lesson.getResource            
			            },
			            blocker: this.blocker
			         
			         };           
          
          
			          moduleManager.next(params);    
			          
			     }  	
      	        
      
            }
             
               this.setItem(i-1);

				    
          
       },

		refresh: function () {
			    this.setItem(i);
		},	
		
		endofLesson: function () {                                                                           // Called on end of lesson, providing the parameters for the moduleManager's lessonEnd event
 			
            i = lesson.length;			
			
			
			   debug.log('Firing end of Lesson for item ' + (i+1), false);
			   
			   
            var frame = document.getElementById('module-lesson-end');
            frame.contentWindow.document.getElementById('content').innerHTML = '';			    
			    
             this.module = "lesson-end";			
             
             
             var items = lesson.object.items;
             
             items.forEach(function (item, index) {
                     
                 var params = {
			          
			            name: item.module,
			            data: item.data,
			            user: courseData.handler(index),
			            frame: frame
			         
			         };          
	         
	
						moduleManager.end(params);			                   
             
             });

		},
		        
	
	   
	   status: {                                                                                       // For the purposes of the UI - get the current item and length of the lesson
	                                                                                                   // for the purposes of the player UI (these are called by AngjularJS in the view)
	        getItem: function () {
	        	return i;
	         },
	       
	        getLength: function () {
	  
	        	return lesson.length;
	        } 
	   },
	   
	   
      isBlocked: function () {
      
       return blocked;
      },	   
	   
	   blocker: (function () {                                                                     // Exposes a blocker object for modules which request blocker. Blocker allows a module to temporarily
	                                                                                               // block the user from temporarily switching from the current screen - an intend use is for
	                                                                                                // modules which require some asynchronous loading - to prevent the user from pre-maturely
 	                                                                                                // switching to the next module. Another intended use is error correction - prevent the user from
 	                                                                                                // continuing until they have the correct answer.

				  return {
				    
			 	       block: function () {
	
			 	              blocked = true;
			 	              
			 	              setTimeout(function () {
			 	                blocked = false;
			 	               }, 10000);
			 	       
			 	       },
			 	       
			 	       unblock: function () {
			 
			 	               blocked = false;
			 	       
			 	       }
		 	   
		    	};
			      
	   
	  })()
	
	
	
	
	};
	  	
	
});





