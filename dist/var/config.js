/**
 * Created by sam on 8/21/15.
 */


window.config  = {

    platform: {

        "key": "elite-academy",
        "name": "Elite Academy",
        "color": "#990000",
        "logo": "https://s3-us-west-2.amazonaws.com/teachx/elite-academy/clear-icon.png",
        "storage": {
            "base": "https://s3-us-west-2.amazonaws.com/teachx/elite-academy/",
            "lessons": "lessons/",
            "images": "icons/"
        }

    }

};