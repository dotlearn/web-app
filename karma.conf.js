// Karma configuration
// Generated on Thu Nov 06 2014 20:28:54 GMT-0500 (EST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [

      'src/js/lib/*.js',
      'src/js/lib/zip/zip.js',
      'src/js/lib/zip/zip-ext.js',
      {pattern: 'src/js/lib/zip/inflate.js', watched: true, included: false, served: true},
      {pattern: 'src/il8n/*.js', watched: true, included: false, served: true},
      'src/js/lib/bootstrap/ui-bootstrap.js',
      'tests/jasmine/lib/jasmine-2.0.0/jasmine.js',
      'tests/jasmine/lib/jasmine-2.0.0/jasmine-html.js',
        'tests/jasmine/lib/jasmine-2.0.0/boot.js',
      'tests/jasmine/lib/jasmine-2.0.0/jasmine.css',
      'tests/jasmine/lib/angular-mocks.js',
      'tests/jasmine/spec/*.js',
      'tests/jasmine/mock-lsns/a.js',
      'src/js/*.js'
    ],


    // list of files to exclude
    exclude: [ 'src/js/main.js'
    ],

      proxies :  {
        '/js': 'http://localhost:9876/base/src/js',
        '/il8n': 'http://localhost:9876/base/src/il8n'
      },

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Firefox'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
