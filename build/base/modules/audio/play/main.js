/**
 * Created by sam on 1/16/15.
 */


audiojs.events.ready(function() {
    var as = audiojs.createAll();
});


window.onLoad = function (dotLearn) {

    var app = dotLearn;

    try {

        var content =  document.getElementById('content');
        content.innerHTML = '';



        var loader = document.createElement('img');
        loader.src = 'loading.gif';
        loader.alt = '';
        loader.width = "600";
        loader.height = "415";
        loader.style.display = "block";
        loader.style.margin = "auto";
        loader.style.top = "5%";
        content.appendChild(loader);








        app.blocker.block();

        app.file.get(app.data.location,function (data) {


            var content =  document.getElementById('content');
            content.innerHTML = '';


            var header = document.getElementById('header');
            header.innerHTML = '';

            if(typeof app.data.caption !== 'undefined'){
                header.appendChild(document.createTextNode(app.data.caption));
            } else{

                header.appendChild(document.createTextNode(app.data.location.split('/').pop()));
            }



            var img  = document.createElement('img');

            img.src = 'play.png';

            img.width = 200;

            img.style.position = "relative";

            img.style.margin = "auto";

            img.style.marginTop= "10dp";
            img.style.marginBottom="10dp";
            img.style.display = "block";

            content.appendChild(img);


            var audio = document.createElement("audio");
            audio.className = "viewer";
            audio.controls = true;
            audio.id = "audio";

            audio.src = window.URL.createObjectURL(data);

            audio.style.position =  "relative";
            audio.style.display = "block";
            audio.style.margin = "auto";
            audio.style.marginTop="10dp";

            content.appendChild(audio);
            console.log('audio loaded');



            app.blocker.unblock();


            audio.onended = function(){

            }


        });

    } catch (e) {
        //Something happened
        console.log('Something happened');
        console.log(e);

    }

};


window.onNext = function(){

    var audio = document.getElementById('audio');


    try{

        if(typeof audio !== 'undefined'){

            if (typeof audio !== 'null') {
                audio.pause();
            }
            else {
                //Audio doesn't exist - why?
                console.log('Something is up here - audio is null');
            }

        }  else{
            console.log('Something is up here - audio is undefined');
        }

    }  catch (e) {

        //Some error going on here
        console.log('Something is up here ');

    }


};