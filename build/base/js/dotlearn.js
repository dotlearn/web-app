/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattachatryya

File: dotlearn.js

Description: Creates the dotLearn object, who's primary purpose is to serve as the interface for loading modules into the Web-app, according
to the module API. Modules call this object in their module.js file to register their module and it's functionality.

Module functionality and API is inspired by the module definition systems of angularJS and gruntjs. See readme/wiki for more info

Usage:

dotlearn.module('my-module-name', function(moduleManager){

moduleManager.lessonLifeCycleEvent({configuration object}, function(contextobject){
     
     contextobject.somespecialobject.useSomeSpecialFunction();
     
  });


})

=======================================================================================================================================*/

dotlearn = (function () {

		return {
	
			modules: {},
			
			
			module: function (name, instantiate) {
			
             var module = {};
             
             module.name = name;
             
             var moduleManager = (function () {
                   
                   return {
                   	
                   	     config: function (config) {
                   	           
                   	           module.config  = config;              
                   	     
                   	     },
                   
                          init: function (config, func) {
                             
                                  module.init = { 
                                         config: config,
                                         onInit: func                                  
                                  };                          
                          
                          },
                          
                          onStart: function (config, func) {
                                     
                                  module.start = {
                                        config: config,
                                        onStart: func,
                                  }; 
                          
                          },
                          
                          onLoad: function (config, func) {
                          
                                  module.load = {
                                       config: config,
                                       onLoad: func   
                                  };
                          
                          },
                          
                          onNext: function (config, func) {
                          
                                  module.next = {
                                       config: config,
                                       onNext: func   
                                  };
                          
                          },
                          
                          
                          onEnd: function (config, func) {
                          	
                          	       module.end = {
                                       config: config,
                                       onEnd: func   
                                  };
                          
                          
                          },
                          
                          onEdit: function (config, func) {
                          
                                  module.edit = {
                                       config: config,
                                       onEdit: func   
                                  };
                           
                          }
                          
                   };             
             
             })();			
             
             instantiate(moduleManager);
             
             this.modules[name] = module;
			
			}
               
		  
		};
}
)(); 

