angular.module('modules-ui', []).
directive('styleSheet', function() {                                         // For each module, append a stylesheet to the module's assigned iFrame
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
        
            $head = angular.element(element).contents().find('body');
            
               $head.append('<link rel="stylesheet" type="text/css" href="' + attrs.styleSheet + '" />');
          
            
           angular.element(element).bind('load', function() {
                // cached body element overrides when src loads
                // hence have to cache it again            
                $head = angular.element(element).contents().find('head');
                
 
                $head.append('<link rel="stylesheet" type="text/css" href="' + attrs.styleSheet + '" />');
            }); 
            
            attrs.$observe('updateBodyStyle', function(value) {
        
                             $head.append('<link rel="stylesheet" type="text/css" href="' + attrs.styleSheet + '" />');
                
            }, true);
        }
    };
})
.directive('injectScripts', function() {                             // For each module, inject library scripts into the module's assigned iframe
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
         
        var inject = function () {
             
         
          if(attrs.injectScripts !== ''){     
            
            	 var doc =  element[0].contentWindow.document;

        //       var name = element[0].name.split('-')[1];
                
      //          var path = 'modules/' + name + '/';
            	 
        	       var scripts = util.toArray(attrs.injectScripts, '"');
						
        
				
      
               if(typeof scripts !== 'undefined'){
            
						            	 	
			              if(typeof scripts.forEach !== 'undefined'){
			            	     
 				            	     
						            	 scripts.forEach(function (script) {
						            	 	
					            	  
						            	 					var scr = doc.createElement('script');
						            	 			   	scr.type = "text/javascript";
						            	 			   	scr.src = script;
						            	 			      doc.body.appendChild(scr);
			/*			            	 			   	
						            	 			      var scr2 = doc.createElement('script');
						            	 			   	scr2.type = "text/javascript";
						            	 			   	scr2.src = path + script;
						            	 			   	doc.body.appendChild(scr2);
           */                 
						            	 			   		
						            	         
						            	 });
			            	 
			              }
               
               
               }                
                 
                 
                  }
            	
            };

           angular.element(element).bind('load', function() {
                // cached body element overrides when src loads
                // hence have to cache it again            
                     
                       inject();
                      

            }); 
        /*    
            attrs.$observe('injectScripts', function(value) {
        
                           inject();
                         
                
            }, true);
      */  }
    
  
    };
}).directive('moduleFrames', function ($compile) {

return {
restrict: 'E',
link: function (scope, element, attrs) {




   var modules = scope.modules;

   
   modules.forEach(function (module) {
      var html = document.createElement('iframe');
      var template = angular.element(html);
      
      template.attr('style-sheet', '{{frames[\'' + module +  '\'].css}}' );
		template.attr('ng-src', '{{frames[\'' + module +  '\'].html}}');
		template.attr('id', 'module-' + module);
	   template.attr('name', 'module-'+module);
	   template.attr('width', '480');
	   template.attr('height', '320');
	   template.attr('ng-show', "current ==='"+ module + "'");      
	   template.attr('inject-scripts', '{{frames[\'' + module +  '\'].scripts}}');

      template.on('load', function () {scope.init(module);});
      var linkFn = $compile(template);
      var ele = linkFn(scope);
      element.append(ele);
   
   });
}

};

}).directive('file', function(){                                                            // Each time a file is uploaded, update the model with the file data
    return {
        scope: {
            file: '='
        },
        link: function(scope, el, attrs){
            el.bind('change', function(event){
                var files = event.target.files;
                var file = files[0];
                
                
                scope.file = file;
                scope.$apply();
            });
        }
    };
});

