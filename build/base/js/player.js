/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattachatryya

File: player.js

Description: AngularJS module for handing the lesson player functionality - managing all the core functionality of the lesson player. 
This module interacts heavily with the moduleManager module, trigerring the events of most stages of the module lifecycle.

For more info, check out the info on the lesson Player on the wiki/readme.


=======================================================================================================================================*/

angular.module('player', ['file', 'module', 'user']).
value('lesson', {}).
value('div', document.getElementById('play-item')).
value('i', 0).
value('blocked', false).
factory('player', function (fileHandler, moduleManager, blocked, lessonData) {
	
	return {

		lessonObject: {},


		current: 0,


		getTitle: function () {


			if(this.getCurrent() == this.getLength()) return "Lesson End";
			if(!this.lessonObject) return "";
			if(!this.lessonObject.items) return "";
			if(!this.lessonObject.items[this.current]) return "";


			return this.lessonObject.items[this.current].label;
		},

		getCurrent: function () {

			return this.current;

		},


		getLength: function () {

			if(!this.lessonObject) return 0;
			if(!this.lessonObject.items) return 0;
			return this.lessonObject.items.length;

		},


		onExit: function () {

		},


		next: function () {

			if(this.current < (this.getLength()-1)) {
			        this.onNext();
					this.setItem(this.current +1);
			}
			else if(blocked) return {};
			else if(this.current == (this.getLength()-1)) this.lessonEnd();
			else  this.onExit();


		},

		previous: function () {

			if(this.current > 0) {
				this.onNext();
				this.setItem(this.current -1);

			}
		},


		viewPanel: document.getElementById('preview-frame-container'),

		
		loadFromServer: function (lesson, success, error, progress) {



			var url = config.platform.storage.base + config.platform.storage.lessons + lesson.id + '.lsn';



			this.lessonObject = lesson;
			var player = this;

			download(url, function(file){

				util.debug.log('Downloaded lesson file from the server');

				util.debug.log('Opening  / Importing the file');

				openFile(file, function () {


					success();
					player.setItem(0);
					lessonData.init(lesson.id);

				}, error, progress);



			});



			function download(url, callback){


				var xhr = new XMLHttpRequest();

				xhr.open("GET",url, true);

				xhr.responseType = "arraybuffer";

				xhr.onload = function(oEvent) {

					var blob = new Blob([oEvent.target.response], {type: 'application/zip'});
					blob.name = lesson.id+ '.lsn';

					callback(blob);

				};

				xhr.onprogress = function (oEvent) {

					if (oEvent.lengthComputable)
						progress(Math.round((oEvent.loaded / oEvent.total)*100));

				};

				xhr.onerror = function (oEvent) {
					error(oEvent);
				};

				xhr.send();


			}



			function openFile(file, callback, onerror, onprogress){

				fileHandler.open(file, function (){

					util.debug.log('Downloaded file successfully opened');


					callback();

				}, onerror);
			}

		},



		onNext: function () {

			if(this.current == this.getLength()) return null;


			var iframe = document.getElementById('preview');

			var itemObject = util.strip(this.lessonObject.items[this.current]);

			var dotLearnObject = this.createDotLearnObject(itemObject);

			if(iframe){

				if(iframe.contentWindow){



					if(iframe.contentWindow.onNext) iframe.contentWindow.onNext(dotLearnObject);

				}

			}


		},




		setItem: function(index){




			if(blocked) return {};

			this.current = index;

			var itemObject = util.strip(this.lessonObject.items[index]);

			var modules = moduleManager.getModules();
			$(this.viewPanel).empty();


			var iframe = document.createElement('iframe');
			iframe.id = 'preview';
			iframe.className = 'player';
			iframe.setAttribute('allowFullScreen', '');

			iframe.src="modules/"+itemObject.module + "/" + modules[itemObject.module].main;



			var viewObject = this.createDotLearnObject(itemObject);

			iframe.onload = function () {
				iframe.contentWindow.onLoad(viewObject);
			};

			this.viewPanel.appendChild(iframe);
		},
		


		lessonEnd: function () {

			$("#lesson-end-container").empty();

			this.current = this.getLength();

			var items = this.lessonObject.items;
			var modules = moduleManager.getModules();
			var createDotLearnObject = this.createDotLearnObject;

			var container = document.getElementById("lesson-end-container");

			items.forEach(function (item) {

				var module = modules[item.module];


				if(module.end){

					var iframe = document.createElement('iframe');
					iframe.id = 'frame-'+ item.id;
					iframe.className = 'lessonEnd';
					iframe.style.width = "300px";
					iframe.style.height = "200px";
					iframe.style.boxShadow = "2px 2px 3px #888888";
					iframe.style.margin = "24px";
					iframe.style.position="relative";
					iframe.style.display= "block";
					iframe.style.float="left";



					iframe.src="modules/"+item.module + "/" + module.end;




					container.appendChild(iframe);


					iframe.onload = function () {

						if(iframe.contentWindow.onLoad) iframe.contentWindow.onLoad(createDotLearnObject(item));
					};




				}




			});


		},

		
		createDotLearnObject: function (item) {

			return {

				file: {


					get: function(fileName, callback){

						fileHandler.getFile('res/' + item.id + '/' + fileName, callback);
						//callback(workspace.getResourceByItem(item.id, fileName));
					},


					inModule: function(fileName, callback){

						fileHandler.getFile('res/modules/' + item.module + '/' + fileName, callback);
						//callback(workspace.getResourceByModule(item.module, fileName));


					}


				},

				data: item.data,


				lang: '',


				blocker: {
					block: function(){

						blocked = true;

						setTimeout(function () {
							blocked = false;
						}, 5000);
					},

					unblock: function(){
						blocked = false;
					}
				},

				user: {

					get: function(){


						return lessonData.get(item.id);

					},

					set: function(object){

						console.log('setting object' + object);
						lessonData.set(item.id, object);
					}
				}
			};
		}
		


	};
	  	
	
});


