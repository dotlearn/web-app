
/*
angular.module('debug', [])
.value('messages', []).
factory('debug', function ($log, messages) {

if (!Date.now) {
    Date.now = function() { return new Date().getTime(); };
}

   return {
   	
   	 init: Date.now(),
   
        warn: function (msg, toConsole) {
        
            if(typeof toConsole === 'undefined'){
                toConsole = true;
            }   
        
            if(toConsole){
              $log.warn(msg);
            }
        
            var stamp = Date.now() - this.init;
            
            var object = {
               text: msg,
               time: stamp,
               type: 'warn'
            };
            
            messages.push(object);

        },
        
        notify: function (msg, toConsole) {
        	
        	               if(typeof toConsole === 'undefined'){
                toConsole = true;
            }         
        	          if(toConsole){
        	    $log.notify(msg);
        	    
        	    }
            
            var stamp = Date.now() - this.init;
            
            var object = {
               text: msg,
               time: stamp,
               type: 'notify'
            };
            
            messages.push(object);

        },

        info: function (msg, toConsole) {
        	
        	                if(typeof toConsole === 'undefined'){
                toConsole = true;
            }         
                        if(toConsole){
                      	    $log.info(msg);
                      	    }
            
            var stamp = Date.now() - this.init;
            
            var object = {
               text: msg,
               time: stamp,
               type: 'info'
            };
            
            messages.push(object);

        },
        
        log: function (msg, toConsole) {
        	
            if(typeof toConsole === 'undefined'){
                toConsole = true;
            }    
        	    
        	              if(toConsole){
        	
        	  $log.log(msg);
        	  
        	  }
            
            var stamp = Date.now() - this.init;
            
            var object = {
               text: msg,
               time: stamp,
               type: 'log'
            };
            
            messages.push(object);

        },
        
        report: function () {
        
           return JSON.stringify(messages);
                    
        }   

   
   };


});

    */