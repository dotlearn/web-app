/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattachatryya

File: library.js

Description: AngularJS module for loading and setting Library data, which is done via an object store in IndexedDB. This allows the
course install / uninstall functionality. The install/uninstall functionality is related to, but distinct and incopmatible with
library.js in the course creator app, which uses a different object format

=======================================================================================================================================*/

angular.module('library', []).
	value('server', 'http://lpaasfrontend-dotlearn.rhcloud.com/').
factory('library', function ($http, server) {

	return {
  						
        lessons: [],


		tree: {},


		current: '0',


		getCategories: function(){


			console.log(this.tree);


			if(!this.tree['0']) return [{}];

			if(!this.tree['0'].children) return [{}];

			return this.tree['0'].children;

		},


		getCategory: function (category) {

			if(!this.tree[category]) return {};

			return this.tree[category];

		},

		getLessons: function(category){

			if(!this.tree[category]) return [{}];

			if(!this.tree[category].children) return [{}];


			return this.tree[category].children;
		},


	   refresh: function (callback) {

		   var target = server + 'getLessons';


		   var tree = this.tree;
		   var handler = this;
		   var buildTree = this.buildTree;

	     $http.post(target, {client_id: config.platform.key}).success(function(data, status, headers, config) {


			 handler.tree = buildTree(data);

			 callback(null, tree);

		   }).
			   error(function(data, status, headers, config) {


				 callback(data);
			   });

	   },


		buildTree: function(nodes) {

			var NodesList = {"0": {type: 'root', children: []}};

			if (!nodes.length) return NodesList;
			if (nodes.length < 1) return NodesList;

			// First Pass
			nodes.forEach(function(node){
				NodesList[node.id] = node;
			});

			//SecondPass
			nodes.forEach(function(node){
				if(NodesList[node.id].children){

					NodesList[node.id].children.forEach(function(childID, i){
						var child = NodesList[childID];

						if(child){

							child.parent = node.id;
							NodesList[node.id].children[i] = child;


						}


					});
				}
			});

			//ThirdPass
			nodes.forEach(function(node){
				if(NodesList[node.id].type === "category"){
					NodesList["0"].children.push(NodesList[node.id]);
				}
			});

			return NodesList;

		}

                 
		
	};


});

