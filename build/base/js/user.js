/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattachatryya

File: user.js

Description: AngularJS module for managing user data. Uses nothing more than local storage to store data. It manages two types of data:
1) User records/data for each lesson and course accesses
2) User preferences for the software itself, such as language and several other options

=======================================================================================================================================*/


angular.module('user', []).

factory('user',  function (preferences, records) {
return {

    init: function (data) {
    	   preferences = data.preferences;
    	   records = data.records;

    },
    
    rec: {                                                // Stores user records (answers, progress) for each lesson / course used

        				 set: function (id, object) {
       				 	
        					   records[id] = object;
 
        				 },
        			
        				 get: function (id) {

							 if (typeof records[id] == 'undefined') {
								 this.set(id, {});

							 }

							 return records[id];
						 }

    
    
    },
    
    
    pref: {
    	
    	  set: function (key, value) {
    	     
    	     preferences[key] = value;

    	     
    	  
    	  },
    	  
    	  get: function (key) {
    	  
    	     return preferences[key];
    	     
    	  }
    
    },
    
    save: function () {
    	

       var data = {};
       data.preferences = preferences;
       data.records = records;
       localStorage.user = JSON.stringify(data);
    }
    

};

}).
factory('userPreferences', function (user) {

return {

    set: function (key, value) {
    
       user.pref.set(key, value);
       user.save();
    },
    
    get: function (key) {
    	
    	if(typeof user.pref.get(key) === 'undefined'){
    	   user.pref.set(key, {});
    	   user.save();
    	}

		return     user.pref.get(key);
    }
};

}).

factory('lessonData',  function (user) {        // Interface between the record store object 'user', and the player module. The 'user' object just sets, gets, saves etc.. the CourseData object formats in a way that
                                                       // is useful for the player module. 

return {



	init: function (lessonID) {

		this.currentID = lessonID;
		this.lesson = user.rec.get(lessonID);
	},

	currentID: '0',

	lesson:{},


    get: function (itemID) {

    		 if(typeof  this.lesson[itemID] == "undefined" )  return {};

		     return this.lesson[itemID];
    },
    
    set: function (itemID, object) {



    	   if(typeof object == "undefined") object = {};

	       	this.lesson[itemID] = object;
    	    this.save();

    },
    
    save: function () {
         user.rec.set(this.currentID, this.lesson);
         user.save();
    }
    

    
    
};

}).

run(function (user) {


		if(localStorage.user) user.init(JSON.parse(localStorage.user));


}).

value('records',{}).
value('preferences', 
{      language:  '',
       storeData: true,
});



