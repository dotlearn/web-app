angular.module('dotlearn-ui', []).
directive('xbutton', function () {

return {
  restrict: 'E',
  transclude: true,
  scope: {
   
    color: '='
  
  },

	template: '<div  class="button"><div ng-transclude></div></div>',
   link: function (scope, element, attrs) {
   
   

      element.children().css('background-color', scope.color);
   }
};

}).directive('panelHeader', function ($compile) {

return {
       restrict: 'E',
       link: function (scope, element, attrs) {
       	
         var x =  '<div class="top-bar"><div class="panel-title">{{g[\''+attrs.name+'\']}}</div><div class="close-button" ng-click="$parent.screen= \'\'"><img src="icons/close.png" alt=""></img></div></div> ';
         var template = angular.element(x);
         var linkFn = $compile(template);
         var ele = linkFn(scope);
         element.append(ele);
       
       }

};

}).directive('tabs', function ($compile, userPreferences, intl) {

return {

	restrict: 'E',
	require: '?ngModel',

	link: function (scope, element, attrs, ngModel) {
	
         element.addClass('tab-bar');	
         
         
         var x = window.getComputedStyle(element[0]).width;
         var str = x.split('p')[0];
         var width = parseInt(x);  
         
   
       var lang = userPreferences.get('language');	
	
     intl.setLanguage(lang, function () {
               
                        var values = attrs.values;
         
         var array = util.toArray(values, "'");
         
         var y = Math.floor(width/array.length);
    
         
         array.forEach(function (item) {
           
               var tab = '<div class="tab" ng-click="'+ attrs.ngModel + '= \'' + item + '\'"  ng-class="{\'tab-selected\': (' + attrs.ngModel + '===\''+item +'\')}"><div class="hilight" ng-show="' + attrs.ngModel + '===\'' + item + '\'"></div>' + intl.map[item] + '</div>';
               
               var template = angular.element(tab);
               template.css('width', y+'px');         
               
               var linkFn = $compile(template);
               var ele = linkFn(scope);
                 
              element.append(ele);
              
              util.safeApply(scope);          
         
         });	      
	             
	               
	               
	      
	      });
	



	
	}



};



}).directive('previewImage', function(){
    return {
        scope: {
            previewImage: '='
        },
        link: function(scope, el, attrs){
            el.bind('change', function(event){
                var files = event.target.files;
                var file = files[0];
                
                var blob = new Blob([file], {type:file.type});
                blob.name = file.name;
                     
							scope.previewImage ={
								     blob: blob,
							        url: URL.createObjectURL(blob)
							}; 
					
							scope.$apply();
      
            });
        }
    };
});

