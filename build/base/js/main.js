/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattachatryya

File: main.js

Description: A simple javascript file which uses LadyLoad to load all the requires javascript files

=======================================================================================================================================*/


Modernizr.load([
{ 
 test: Modernizr.indexeddb,
 nope: 'js/lib/IndexedDBShim.min.js'

}
]);





LazyLoad.js(['js/lib/jquery/jquery.js','js/lib/zip/zip.js', 'js/lib/zip/zip-ext.js', 'js/lib/angular/angular.min.js', 'js/lib/async.min.js', 'js/app.js', 'js/library.js', 'js/util.js', 'js/loader.js', 'js/file.js', 'js/player.js', 'js/module.js', 'js/user.js'], function () {
    //^ Functional Modules

  	   	             zip.workerScriptsPath = "js/lib/zip/";
	
							  	if (!Modernizr.webgl){
										 LazyLoad.js(['js/lib/zip/typedarray.js', 'js/lib/zip/inflate.js'], function(){  
										 zip.useWebWorkers = false;
										 console.log('using polyfill');
										});			
									}							
		
									  

window.URL = window.URL || window.webkitURL;	
     	
});


			
			