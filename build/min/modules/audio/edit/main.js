/**
 * Created by sam on 1/16/15.
 */
var app = angular.module('app', [])
    .config( [
        '$compileProvider',
        function( $compileProvider )
        {
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|blob):/);
            // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
        }
    ]).
    controller('mainController', function ($scope) {



        i18n.init(function (err, t) {
            $scope.t = t;
            $scope.$apply();
        });



        $scope.data = {

            file: {}
        };

        $scope.item = {

        };

        $scope.file = {};


        $scope.setFile = function (file) {

        $scope.file = file;

            var ext = file.name.split(".")[1];

            switch(ext){

                case "mp3":
                    if((typeof $scope.item.data.location !==  "undefined") && ($scope.item.data.location !== "")  ){
                        $scope.fileLoader.item.remove($scope.item.data.location);
                        $scope.item.data.location = "";
                    }
                    $scope.item.data.location = file.name;
                    break;
                case "jpg":
                case "png":
                    $scope.item.data.image = file.name;
                    break;

                default:
                    return null;
                    console.log("invalid file type");

            }

            $scope.fileLoader.item.set(file.name, file);

            $scope.preview();

        };






        window.onLoad = function (dotLearn) {

            $scope.item.data = dotLearn.data;

           $scope.fileLoader = dotLearn.file;



            $scope.$apply();


            $scope.preview();



        };


        window.onFinished = function () {

            return JSON.parse(JSON.stringify($scope.item.data));
        };




        $scope.preview = function () {


            if((typeof $scope.item.data.location !==  "undefined") && ($scope.item.data.location !== "")  ){

            var file = $scope.fileLoader.item.get($scope.item.data.location);


            var audio = document.createElement("audio");

            audio.src = URL.createObjectURL(file);
            audio.controls = true;

            document.getElementById("audio-container").appendChild(audio);


            if(typeof $scope.item.data.image !== 'undefined'){

                var imageFile = $scope.fileLoader.item.get($scope.item.data.image);

                console.log(imageFile);
                $scope.image = URL.createObjectURL(imageFile);
                console.log($scope.image);
                $scope.$apply();

            }

            }
        };

    }).directive('previewImage', function(){
    return {
        scope: {
            previewImage: '='
        },
        link: function(scope, el, attrs){
            el.bind('change', function(event){
                var files = event.target.files;
                var file = files[0];

                scope.file = file;

                scope.$parent.setFile(file);

                scope.$apply();

            });
        }
    };
});