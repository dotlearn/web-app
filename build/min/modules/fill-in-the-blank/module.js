dotlearn.module('fill-in-the-blank', function (moduleManager) {

			
			moduleManager.init({}, function (app) {
			
			});
			
         moduleManager.onLoad({user:true}, function (app) {
         	
         	
         	   var header = document.getElementById('header');
         	   header.innerHTML = '';
           			
           		var container = document.getElementById('content');   
         		container.innerHTML = '';
         		
         		 var data = app.data;
         		 
         		 var text = document.createTextNode(data.prompt);   //
									  					
  					
  					   var div = document.createElement('div');  // Add the prompt
  			         div.style.fontWeight = 'bold';		   
           					   
           			div.appendChild(text);
           			
                  header.appendChild(div);
           			
           			
           			var input = document.createElement('input');           			
           			input.type = 'text';
           			input.id = 'fill-in-the-blank-response';
           			
           			if(typeof app.user.get() == 'string'){
           				

           				                  input.value = app.user.get();   
           	
                       			
           			}
           			
           			input.style.position = 'relative';
           			input.style.float = 'right';
           			input.style.top = '50px';
           			
           			
           			container.appendChild(input);
           			
         
         
         });
			
      	moduleManager.onNext({user:true}, function (app) {
      	
                     var response = document.getElementById('fill-in-the-blank-response');
                         
                       app.user.set(response.value);	 		

      	});
      	
         moduleManager.onEnd({frame:true, user:true}, function (app) {
         	
         	               var frame = app.frame;
         	
         			
				                var answerbox = document.createElement('div');
					    		 	 answerbox.className = 'answer-box';
					    		 	 
					    		 	 var answertext = document.createElement('div');
					    		 	 answertext.className = 'answer-question';
					    		 	 answertext.appendChild(document.createTextNode(app.data.prompt));
					    		    answerbox.appendChild(answertext);
					
				
					    		   var response = app.user.get();
					    		   var answers = app.data.answers;
					 
			                  console.log(app.data);
	
		                    
                           var correct = false;                             
                             		                     
		                     answers.forEach(function (answer) {
		    
		                             if(response == answer.text){
                      							correct = true;		                             
		                             }
		                     
		                     });
		                          
		                          

					
											var yesno = document.createElement('div');
											yesno.className = 'answer-yesno';	    		
					    		
											if(correct){							
													yesno.appendChild(document.createTextNode('Correct'));								
												}
												else {
													yesno.appendChild(document.createTextNode('Incorrect'));												
												}
					    		  		answerbox.appendChild(yesno);    
					    	
					    	
					    		  	 var explanation = document.createElement('div');
									 explanation.className = 'answer-text';
									 explanation.appendChild(document.createTextNode(app.data.explanation));
									 answerbox.appendChild(explanation);		
                                 					    		  		
					    		  		
									frame.appendChild(answerbox);
         
         
         });
         
         moduleManager.onEdit({template:'fillintheblank.html', structure: {
	         			prompt: 'What is the symbol for iron on the periodic table?',
	         			answers: [{text: 'Fe'}]
         
          }}, function (app) {
         
         });


});