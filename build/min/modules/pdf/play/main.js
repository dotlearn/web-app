/**
 * Created by sam on 1/18/15.
 */


var pdf;

var pageNumber;

var container = document.getElementById('pdf-preview-panel');


window.onLoad = function (app) {

    document.getElementById("caption").appendChild(document.createTextNode(app.data.caption));

    app.file.get(app.data.location, function (data) {

        var fileReader = new FileReader();
        fileReader.onload = function () {

            var array = new Uint8Array(this.result);

            PDFJS.getDocument(array).then(function (pdfObject) {


                 pdf = pdfObject;


                showPage(1);

            });


        };
        fileReader.readAsArrayBuffer(data);


    }, function (e) {

    });

};


$(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e)
{
    var fullscreen = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;

    if(!fullscreen)
        exitFullScreen();

});

var tryFullScreen = function () {


    var element = document.getElementById("pdf-preview-panel");
    document.getElementById("close").style.visibility = "visible";
    document.getElementById("pdf-info").style.fontSize = "20px";
    document.getElementById("caption").style.fontSize = "20px";
    document.getElementById("pdf-container").style.top= "30px";

    launchIntoFullscreen(element);



};





window.back = function () {

    if(pageNumber > 1)
        showPage(pageNumber-1);
};

window.next = function () {

    if(pageNumber < pdf.pdfInfo.numPages)
        showPage(pageNumber+1);
};



var navigate = function (event) {

    if(event.keyCode == 37)
        back();

    if(event.keyCode == 39)
        next();

};



var exitFullScreen = function () {

    document.getElementById("close").style.visibility = "hidden";
    var element = document.getElementById("pdf-preview-panel");
    document.getElementById("pdf-info").style.fontSize = "12px";
    document.getElementById("caption").style.fontSize = "12px";
    document.getElementById("pdf-container").style.top= "25px";

    exitFullscreen(element);

};



var  showPage = function (number) {


    if(number == 1) {
        document.getElementById("back-button").style.visibility = "hidden";
    } else{
        document.getElementById("back-button").style.visibility = "visible";
    }

    if(number == pdf.pdfInfo.numPages) {
        document.getElementById("front-button").style.visibility = "hidden";
    } else{
        document.getElementById("front-button").style.visibility = "visible";
    }

    if (!pdf)
        return console.log("No PDF set");

    pageNumber = number;

    pdf.getPage(number).then(function(page) {

        var scale = 1;
        var viewport = page.getViewport(scale);

        var canvas = document.getElementById('pdf-container');
        var context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        document.getElementById("pdf-info").innerHTML = "Page number: " + pageNumber + "/" +  pdf.pdfInfo.numPages;


        var renderContext = {
            canvasContext: context,
            viewport: viewport
        };
        page.render(renderContext);



    });

};


function launchIntoFullscreen(element) {
    if(element.requestFullscreen) {
        element.requestFullscreen();
    } else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    } else if(element.msRequestFullscreen) {
        element.msRequestFullscreen();
    }
}

function exitFullscreen() {
    if(document.exitFullscreen) {
        document.exitFullscreen();
    } else if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}



window.onNext = function (app) {

};