/**
 * PDF Edit Module
 * Created by sam on 1/16/15.
 *  Snow
 */

var app = angular.module('app', []).
    controller('mainController', function ($scope) {

        i18n.init(function (err, t) {
            $scope.t = t;
            $scope.$apply();
        });


        $scope.pageNumber =0;

        $scope.data = {
            file: {}
        };

        $scope.item = {};

        $scope.file = {};

            $scope.nextable = function () {

                if($scope.pdf){
                    if     ($scope.pageNumber < $scope.pdf.pdfInfo.numPages){
                        return true;
                    }
                }
                return false;
            };

            $scope.backable = function () {

                if($scope.pdf){
                    if     ($scope.pageNumber >1){
                        return true;
                    }
                }
                return false;
            };


            $scope.back = function () {
                if($scope.pageNumber > 1)
                     $scope.showPage($scope.pageNumber-1);
            };

            $scope.next = function () {
                if($scope.pageNumber < $scope.pdf.pdfInfo.numPages)
                    $scope.showPage($scope.pageNumber+1);
            };



            $scope.showPage = function (number) {
               if (!$scope.pdf)
                    return console.log("No PDF set");

                $scope.pageNumber = number;

                $scope.pdf.getPage(number).then(function(page) {

                    var scale = 1;
                    var viewport = page.getViewport(scale);

                    var canvas = document.getElementById('pdf-container');
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;

                    var renderContext = {
                        canvasContext: context,
                        viewport: viewport
                    };
                    page.render(renderContext);

                    util.safeApply($scope);



                });



            };



        $scope.exitFullScreen = function () {

            $scope.fullscreen = false;
            $scope.fullScreenText = {};
            $scope.fullScreenContainer = {};
            $scope.fullScreenExit = {};

            var element = document.getElementById("pdf-preview-panel");

            exitFullscreen(element);

            util.safeApply($scope);

        };




        $scope.preview = function () {

            if((typeof $scope.item.data.location !==  "undefined") && ($scope.item.data.location !== "")  ){


                var file = $scope.fileLoader.item.get($scope.item.data.location);

                console.log(file);

                var fileReader = new FileReader();
                fileReader.onload = function() {

                    var array = new Uint8Array(this.result);

                    PDFJS.getDocument(array).then(function(pdf) {


                        $scope.pdf = pdf;


                        $scope.showPage(1);

                    });


                };
                fileReader.readAsArrayBuffer(file);


            }



        };


        $scope.setFile = function (file) {



            $scope.file = file;

            if(file.name.split(".")[1] == 'pdf'){


                if((typeof $scope.item.data.location !==  "undefined") && ($scope.item.data.location !== "")  ){
                    $scope.fileLoader.item.remove($scope.item.data.location);
                    $scope.item.data.location = "";
                }

                $scope.item.data.location = file.name;
                $scope.fileLoader.item.set(file.name, file);

                $scope.item.data.caption = util.strip(file.name);

                console.log(file);
                console.log($scope.item.data.location);

                $scope.preview();

            } else{

                alert("Invalid file type - please use a PDF file");
                $scope.screen="init";
            }




        };




        $(document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e)
        {
            var fullscreen = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;

            if(!fullscreen)
                $scope.exitFullScreen();

        });


        $scope.tryFullScreen = function () {

            $scope.fullscreen = true;




            $scope.fullScreenText = {
                'font-size': '24px',
                'margin': '10px'
            };

            $scope.fullScreenContainer = {

                top: '50px'

            };

            $scope.fullScreenExit = {
                visibility: 'visible'
            };


            var element = document.getElementById("pdf-preview-panel");

            launchIntoFullscreen(element);

            util.safeApply($scope);

        };


        $scope.navigate = function ($event) {

            if($event.keyCode == 37)
                    $scope.back();

            if($event.keyCode == 39)
                    $scope.next();

        };


        window.onLoad = function (dotLearn) {

            $scope.item.data = dotLearn.data;

            $scope.fileLoader = dotLearn.file;


           $scope.preview();

            util.safeApply($scope);

        };



        window.onFinished = function () {

            return JSON.parse(JSON.stringify($scope.item.data));
        };

    }).directive('previewImage', function(){
        return {
            scope: {
                previewImage: '='
            },
            link: function(scope, el, attrs){
                el.bind('change', function(event){
                    var files = event.target.files;
                    var file = files[0];

                    scope.file = file;

                    scope.$parent.setFile(file);

                    util.safeApply(scope);

                });
            }
        };
    });


function launchIntoFullscreen(element) {
    if(element.requestFullscreen) {
        element.requestFullscreen();
    } else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    } else if(element.msRequestFullscreen) {
        element.msRequestFullscreen();
    }
}

function exitFullscreen() {
    if(document.exitFullscreen) {
        document.exitFullscreen();
    } else if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}


/*
 $scope.setFile = function (file) {

 $scope.file = file;

 $scope.item.data.caption = util.strip(file.name);


 if(file.name.split(".")[1] == 'pdf'){

 var fileReader = new FileReader();
 fileReader.onload = function() {

 var array = new Uint8Array(this.result);

 PDFJS.getDocument(array).then(function(pdf) {


 $scope.pdf = pdf;

 console.log(pdf);

 $scope.showPage(1);

 });


 };
 fileReader.readAsArrayBuffer(file);

 } else{

 console.log("Invalid file type - please use an mp4 file");
 }
 */