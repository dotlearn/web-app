/**
 * Created by sam on 1/16/15.
 */
var app = angular.module('app', []).
    controller('mainController', function ($scope) {



        $scope.item = {};
        $scope.selected = 0;

        $scope.newOption = function () {

            var text = t("answer") + " " + ($scope.item.data.options.length+1);

            $scope.item.data.options[$scope.item.data.options.length] = {
                            text: text
            };

            $scope.selected = $scope.item.data.options.length-1;
        };


        $scope.setImage = function (file) {

            $scope.file = file;

            var ext = file.name.split(".")[1];

            switch(ext){
                case "jpg":
                case "png":

                    if((typeof $scope.item.data.image !==  "undefined") && ($scope.item.data.image !== "")  ){
                        $scope.fileLoader.item.remove($scope.item.data.image);
                        $scope.item.data.image = "";
                    }

                    $scope.item.data.image = file.name;
                    break;

                default:
                    return null;
                   alert(t("invalidFile"))

            }

            $scope.fileLoader.item.set(file.name, file);

        };


        $scope.setHintImage = function (file) {


            $scope.hintFile = file;

            var ext = file.name.split(".")[1];

            switch(ext){
                case "jpg":
                case "png":

                    if((typeof $scope.item.data.hint.image !==  "undefined") && ($scope.item.data.hint.image !== "")  ){
                        $scope.fileLoader.item.remove($scope.item.data.hint.image);
                        $scope.item.data.hint.image = "";
                    }

                    $scope.item.data.hint.image = file.name;
                    break;

                default:
                    return null;
                    alert(t("invalidFile"));

            }

            $scope.fileLoader.item.set(file.name, file);

        };


        window.onLoad = function (dotLearn) {



            i18n.init(function (err, t) {


                $scope.item.data = dotLearn.data;

                $scope.item.data.prompt = t("question");
                $scope.item.data.options[0].text = t("answer") + " 1";


                $scope.fileLoader = dotLearn.file;


                $scope.$apply();


                $scope.selected = 0;


                $scope.t = t;
                window.t = t;
                $scope.$apply();
            });


        };



        window.onFinished = function () {


            return JSON.parse(JSON.stringify($scope.item.data));
        };


    }).directive('previewImage', function(){
    return {
        scope: {
            previewImage: '='
        },
        link: function(scope, el, attrs){
            el.bind('change', function(event){
                var files = event.target.files;
                var file = files[0];

                scope.file = file;

                scope.$parent.setImage(file);

                scope.$apply();

            });
        }
    };
}).directive('hintImage', function(){
        return {
            scope: {
                hintImage: '='
            },
            link: function(scope, el, attrs){
                el.bind('change', function(event){
                    var files = event.target.files;
                    var file = files[0];

                    scope.file = file;

                    scope.$parent.setHintImage(file);

                    scope.$apply();

                });
            }
        };
    });