/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattachatryya

File: app.js

Description: This is the main javascript file for the HTML5 Webapp - and represents the container module for AngularJS for the app.
This app also includes all the AngularJS controllers and directives - outlining the app's user interface logic.

=======================================================================================================================================*/


var app = angular.module('app', ['library', 'player', 'file', 'loader', 'module']).config( [
    '$compileProvider',
    function( $compileProvider )                                                // Allow loading images from blobs contained in .lsn  and .crs files
    {  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|data:image|data:application\//); }   
]);



app.controller('UIController', function ($scope, $http, library, player) {                                   // The main UI controller for the app - switching between activity panels



	    function setupUI(callback){

			$scope.setCategory = function(categoryID){

				$scope.category = library.getCategory(categoryID);
				$scope.lessons = library.getLessons(categoryID);

			};


			 $scope.player = player;




			$scope.isSelected = function(categoryID){

				if(!$scope.category) return {};

				if(categoryID == $scope.category.id) return $scope.theme.hilight;
				else return {};
			};



			$scope.getItemIcon = function(index){

				var items = $scope.lesson.items;

				var item = items[index];


				var stage;
				var type;

				if(items.length == 1) stage = "standalone";
				else{
					if (index===0) stage = "first";
					else if(index == (items.length-1)) stage = "last";
					else stage = "mid";
				}


				switch (item.module){
					case "video":
					case "audio":
					case "audio-slideshow":
						type = "media";
						break;
					case "html":
					case "image":
					case "pdf":
						type = "doc";
						break;
					case "quiz-multiple":
					case "fill-in-the-blank":
						type="exercise";
						break;
					default :
						type = "exercise";
				}


				return "icons/sprite_" + type + "_" + stage + ".png";





			};


			$scope.progress = 0;


			$scope.downloadProgress = function () {

				console.log($scope.progress);
				return ($scope.progress*3) + 'px';
			};





			$scope.playLesson = function(lesson){

				$scope.progress = 0;

				$scope.window = "lesson";
				$scope.lesson = lesson;
				$scope.loaded = false;


				player.loadFromServer(lesson, function () {


					$scope.loaded = true;
					util.safeApply($scope);

				}, function (err) {

					alert(err);

				}, function (progress) {


					console.log("progress is: " + progress);

					$scope.progress = progress;
					util.safeApply($scope);
				});


				player.onExit = function(){
					$scope.window = "browser";
					$scope.progress = 0;
					$scope.player.current = 0;
					util.safeApply($scope);

				};


				$scope.currentItem = function (index) {

					if(index==player.getCurrent()) return {'color': '#66CCFF'};
					else return {};
				};


			};



			callback();
		}


	    function getLessons(callback){

			library.refresh(function(err, tree){


				if(err) return callback(err);

				$scope.tree = tree;
				$scope.categories = library.getCategories();


				if($scope.categories.length > 0){
					$scope.setCategory($scope.categories[0].id);
				}


				callback(null);



			});
		}


        function setPlatform(callback){

			console.log(platform);

			$scope.platform = config.platform;




			$scope.theme = {

				color: {'background-color': config.platform.color},
				hilight: {'background-color': util.colorLightener(util.colorLightener(config.platform.color))}

			};


			callback();




		}


	    function setLanguage(callback){

			callback();
		}




	   async.parallel([getLessons, setPlatform, setLanguage, setupUI], onSetup);


	   function onSetup(err){

		   if(err) return alert("A problem ocurred trying to reach the server");


		   else{

			   util.safeApply($scope);
			   $scope.window = "browser";


		   }



	   }









});

/*

app.controller('BrowseController', function ($scope, fileHandler, playerHandler, debug) {

   $scope.fileHandler = fileHandler;

   $scope.lessons = [];

   $scope.$watch('fileHandler.lessons', function () {                      // Update the view whenever the list of lessons is changed

	                   $scope.lessons = fileHandler.lessons;
   });
   
   
   $scope.play = function (selected) {                                     // Play the selected lesson
       
       debug.info('Playing lesson # ' + (selected+1) + ' of course: ' + fileHandler.getJSON().name);   
       playerHandler.open(fileHandler.getLesson(selected));
       
       $scope.$parent.screen = 'play';
   };


  $scope.color = function (a, b) {                                      // Change the color of the currently selected lesson in the menu of lessons    
		if (a==b){
	       return {
	          	'background-color': '#66CCFF'     
	       };	
		} else{
			 return {
	          'background-color': 'white'       
	       };	
		}
	};


});

app.controller('ReportController', function ($scope, debug, $http, statusHandler, intl) {

$scope.name = intl.map['your-name'];

$scope.description= intl.map['error-description'];



$scope.$watch("$parent.screen == 'report'", function () {

		util.safeApply($scope, function () {
		$scope.name = intl.map['your-name'];
		
		$scope.description= intl.map['error-description'];
		
		});
});


$scope.report = function () {

	
	var toSend = {
          name: $scope.name,
          description: $scope.description,
          log: debug.report(),
          platform: 'webApp',
          language: intl.current(),
          user: navigator.userAgent
	};
	
	
debug.info('Initializing server report');



$http.post('http://backend-dotlearn.rhcloud.com/', toSend).
  success(function(data, status, headers, config) {
 
          debug.info('Successfully saved data: ' + data);
              statusHandler.notify(intl.map['success-error-report']);
          $scope.$parent.screen = '';
     
  }).
  error(function(data, status, headers, config) {
 
  
             statusHandler.warn(intl.map['problem-error-report']);
             $scope.$parent.screen = '';
  });
  


};



});




app.controller('PreviewController', function ($scope, fileHandler, playerHandler, loader, library, statusHandler, intl, debug) {             // Preview panel takes an uploaded or installed course/lesson, and generates the preview

    $scope.fileHandler = fileHandler;
    
    $scope.id = '';

	 $scope.$watch('fileHandler.preview.name', function () {

	 	$scope.preview = fileHandler.preview;                    // Update the view with the new preview
	 	
		 	if(typeof fileHandler.getBlob().size !== 'undefined'){		 
											 	
						 	library.refresh(function (courses, lessons) {
						 		  util.safeApply($scope);		          
						 	});
		  }
	       
	 }); 	
	 
	 
	 
	 
	 $scope.uninstall = function () {
	 	
	 	
	 	    debug.log('Initiating uninstall of file of type ' + fileHandler.type + ' of name ' + fileHandler.getJSON().name);    
	 
	    
	     library.uninstall(fileHandler.type, $scope.id, function () {  // On remove

	         
	         statusHandler.notify(intl.map['success-remove'] + intl.map[fileHandler.type] + ' ' + fileHandler.getJSON().name);

	         
	         util.safeApply($scope, function () {
	            $scope.$parent.screen = '';        
	         });
	         $scope.installed = false;
	   
	          
	     
	     }, function (e) {               // on Error
	       
	        	  statusHandler.warn(intl.map['error-remove'] + intl.map[fileHandler.type] + fileHandler.getJSON().name);
	     });
	 
	 
	 };
	 

	 $scope.install = function (){
	 	
	 	
	 	        debug.log('Installing file of type ' + fileHandler.type + ' of name ' + fileHandler.getJSON().name);    
	    	 
	    	      	  
	    	      	  
         	  library.install(function () {           // on Success
         	    
         	       
	             statusHandler.notify(intl.map['success-install']  + intl.map[fileHandler.type]+ ' ' + fileHandler.getJSON().name);
           
                 util.safeApply($scope, function () {
                 $scope.$parent.screen = '';
                 });
         	     $scope.installed = true;
         	                     
         	  
         	  }, function (e) {                       // on error
         	  
         	  	 	  statusHandler.warn(intl.map['error-install'] +intl.map[fileHandler.type] + fileHandler.getJSON().name);
         	  });      
             	  


	 
	 };
	                                    
	  
	 
     $scope.open = function () {                         // On opening, file, either send to Lesson Browser (if course), or directly to player (if lesson)
     
        debug.log('Playing file: ' + fileHandler.type + ' of name ' + fileHandler.getJSON().name);    
      
         switch(fileHandler.type) {
             
              case 'course':
              			fileHandler.previewLessons(function () {
              			         util.safeApply($scope, function(){
              			               	$scope.$parent.screen = 'browse';});
              			});	
              break;
              case 'lesson':              
                    playerHandler.open(fileHandler.getLesson(-1));
                    $scope.$parent.screen = 'play';
              break;
         
         }
     
     };
     
     
	 $scope.isInstalled = function () {
		
	      var installed = false;
		
			$scope.id = fileHandler.getJSON().id;	
																	
									var array = [];									
									
									switch(fileHandler.type) {
									
				                   case 'lesson':
				                         array = library.lessons;                   
				                         break;
				                   
				                   case 'course':
				                         array = library.courses;
				                         break;					
									
									}
				                  
				               array.forEach(function (installedFile) {
				              
				                    if ($scope.id === installedFile.id) {
				                              installed = true;
				                    }
				                                          
				               });
				               
	    return installed;

	}; 	 
	 

});

app.controller('FileController', function ($scope, fileHandler, statusHandler, intl, debug) {       
	

	$scope.$watch('file', function () {                                  // When the file in the file input slot changes, then generate a switch from the file upload panel to the preview panel, preview the file
			if(typeof $scope.file != 'undefined'){
				
                    
                     debug.log('Opening file ' + $scope.file.name);                    
                    
							fileHandler.open($scope.file, function () {
								
								   debug.log('Successfully opened ' + $scope.file.name);          
			
		                   	$scope.$parent.screen = 'preview';
			                    
								  	fileHandler.setPreview(function () {
								  	     debug.log('Set Preview', false);     
								  	     
								  	     		util.safeApply($scope);
								  	     	
								 
								  	});	     
			
					}, function (e) {

						
						util.safeApply($scope, function () {
						
						     statusHandler.warn(intl.map['error-file-read'] + ': ' + $scope.file.name + ': ' + intl.map[e.message]);
						});
						
				
								
					});
         			   
			   
			   }
		
	
	}); 

});




app.controller('PlayerController', function ($scope, playerHandler, fileHandler) {           // Controller for the player interface


   $scope.itemIndex = 0;
   $scope.oldItem = -1;


   $scope.player = playerHandler;
   
   
   $scope.reset = function () {
   
      $scope.itemIndex = 0;
      $scope.oldItem = -1;
   
   };
   
   
   $scope.close = function () {
           
           $scope.reset();
           $scope.$parent.screen = '';
   
   };   
   

   
   $scope.preview = function () {

      	      switch(fileHandler.type) {
	        	    case  'course':
	        	            $scope.$parent.screen = 'browse';	 
	        	            break;
	        	    case  'lesson':
	        	       	   $scope.$parent.screen = 'preview';	
	        	            break;
	             }   
              $scope.player.end = false;
              
              $scope.reset();
   
   };
   
   $scope.review = function () {
   	
   	      $scope.player.onNext();
           $scope.itemIndex = playerHandler.status.getLength();
            playerHandler.endofLesson();
            
   
   };
   
   $scope.finish = function () {
   	     switch(fileHandler.type) {
	        	    case  'course':
	        	            $scope.$parent.screen = 'browse';	 
	        	            break;
	        	    case  'lesson':
	        	       	  $scope.$parent.screen = '';	 
	        	       	 
	        	            break;
	             }   
	             
	             $scope.reset();
   
   };
   
   $scope.return = function () {
         playerHandler.setItem(playerHandler.status.getLength() -1);
   
   };

    $scope.set = function () {
    	
    	 if(!playerHandler.isBlocked()){
           
             if($scope.oldItem > -1 ){        
             
                    $scope.player.onNext();
             }                        
           
		       $scope.player.setItem($scope.itemIndex -1);
		       $scope.oldItem = $scope.itemIndex;
       
       } else {
       	$scope.itemIndex = $scope.oldItem;
       }
       //$scope
    
    };

/*
   $scope.player.backwards = function () {                                         // If first item in the lesson, going back returns to lessons menu of course, and to file preview if lesson
   	 if ($scope.player.status.getItem() === 0){
		         switch(fileHandler.type) {
		        	    case  'course':
		        	            $scope.$parent.screen = 'browse';	 
		        	            break;
		        	    case  'lesson':
		        	       	  $scope.$parent.screen = 'preview';	 
		        	            break;
		        }   
   	 } else{	   
     $scope.player.back();
            $scope.player.end = false;
   	 }
   };

   $scope.player.return = function () {                                            // Go back to lessons menu if course, and to file preview if lesson
    
        switch(fileHandler.type) {
        	    case  'course':
        	            $scope.$parent.screen = 'browse';	 
        	            break;
        	    case  'lesson':
        	       	  $scope.$parent.screen = 'preview';	 
        	            break;
        }           

   };
   
      $scope.player.forward = function () {                                                  // If last item in the lesson, close the screen if it's a lesson, or go back to lessons menu if a course
   	 if ($scope.player.status.getItem() == $scope.player.status.getLength()){
	           switch(fileHandler.type) {
	        	    case  'course':
	        	            $scope.$parent.screen = 'browse';	 
	        	            break;
	        	    case  'lesson':
	        	       	  $scope.$parent.screen = '';	 
	        	            break;
	             }   
              $scope.player.end = false;
   	 } else
   	 {
   	        $scope.player.next();
   	 }
   };



});


app.controller('OptionsController', function ($scope, userPreferences) {                   

                              
	$scope.storeData = userPreferences.get('storeData');    


		$scope.$watch('storeData', function () {
		   
		   userPreferences.set('storeData', $scope.storeData);    
		});
});


app.controller('WelcomeController', function ($scope, userPreferences) {



});

app.controller('AboutController', function ($scope) {

$scope.text = 'dot Learn is an app platform for reading e-course files in the .lrn format. E-courses are interactive multimedia courses developed by lecturers and professors available as offline files that you can store on your phone. With this app you can view and access your e-courses offline on your device. The software is currently in beta testing. To get more e-courses check out our website at www.dotlearn.org. ';
  
});

app.controller('LibraryController', function ($scope, library, fileHandler, debug) {             // Controller for the library screen
	
	$scope.library = library;                                                              
	
	$scope.view = '';                                                                     // Determines whether courses or lessons are currently shown on the screen;
	
	$scope.$watch('library.courses.length', function () {

	});

	
	$scope.openLesson = function (index) {                                              

	  var lesson = library.lessons[index];
	  
	  debug.info('Opening lesson from library: ' + index + ' of name ' + lesson.name + ' and id ' + lesson.id);
	  	 
	  fileHandler.import(lesson, 'lesson', function () {
             fileHandler.setPreview(function () {
                   $scope.$parent.screen = 'preview';             
             });	  
	  });
	
	};


	$scope.openCourse = function (index) {
		
			  var course = library.courses[index];
			  
			   debug.info('Opening course from library: ' + index + ' of name ' + course.name + ' and id ' + course.id);
	  
	  fileHandler.import(course, 'course',  function () {
	  	    
             fileHandler.setPreview(function () {
             	
             	 
                   $scope.$parent.screen = 'preview';             
             });	  
	  });
	
	
	};
	


});


app.controller('iFrameController', function ($scope, playerHandler, moduleManager) {        //Each module is assigned an iFrame, generated via angularJS depending on the modules loaded by moduleManager
   
   

    $scope.modules = moduleManager.getModules();  
    $scope.frames = moduleManager.frames;
    $scope.current = moduleManager.getCurrent();
    $scope.getCurrent = moduleManager.getCurrent;
    $scope.player = playerHandler;
    
    $scope.$watch('getCurrent()', function () {
       $scope.current = moduleManager.getCurrent();
       
       util.safeApply($scope);    
    });
 
    
    $scope.init = function (name) {
    	
     var filename = 'modules/' + name + '/module.js';
  	
     LazyLoad.js([filename], function () {
		     moduleManager.init(name, function () {
		     
		       util.safeApply($scope);     
		     });
     
     });

    };
    /*
    $scope.$watch('player.module.length', function () {
       //   console.log($scope.player.module);
      
    });
 
 });






*/

/*
 $scope.view = '';	         // Controls whether or not lessons or courses are visible on the library panel
 $scope.screen = '';        // Controls which activity panel is active at any one time



 $scope.languages = intl.list();



 $scope.userLanguage = '';



 $scope.setLanguage = function (code) {


 if (code == '--') {
 //code = 'en';
 } else{


 $scope.userLanguage = code;

 intl.setLanguage(code, function () {

 $scope.g = intl.map;

 util.safeApply($scope);

 userPreferences.set('language', code);


 });


 }


 };


 $scope.$watch('userLanguage', function () {

 if($scope.userLanguage == '--'){

 $scope.userLanguage = '';

 } else{
 $scope.setLanguage($scope.userLanguage);
 }



 });


 $scope.setLanguage(userPreferences.get('language'));


 util.safeApply($scope);



 $scope.openLibrary = function () {
 $scope.screen = 'library';

 library.refresh(function () {
 util.safeApply($scope);
 });

 };



 $scope.ready = true;


 */