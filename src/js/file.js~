/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattachatryya

File: file.js

Description: This AngularJS module represents the course / lesson object. It's an interface between the loader module and the rest of the 
program. It provides functionality such as loading and verifying a file, generating previews and browsing the lessons of each course


=======================================================================================================================================*/

angular.module('file', ['loader', 'debug']).

value('object', {}).

factory('fileHandler', function (object, loader, lessonHandler, debug) {
	
  
 return  {
 	
 	
 	type: '', 
   	
   open: function (file, callback, onerror) {                                          // Function that is called when the raw file is uploaded
   
		   	 loader.load(file);
		   	 
		   	    
		   	 this.type  = this.identify(file.name);
          
     
		       this.verify(function(json){         // After verifying      			 
		                object = json;
		                                     
		                callback();     
						  }, onerror);     
   },
   
   import: function (stored, type, callback) {                                 // Import the file from the library
     
       loader.setBlob(stored.blob);
       this.type = type;

       this.verify(function (json) {
               object = json;
               callback();
       });
   
   },
      
    identify: function (name) {                                                // Determine if the file is an e-course or an e-lesson file
             
            var type = '';  
             
            var ext = name.split('.').pop();
            
            if(ext == "crs"){
              type = 'course';             
            } else if(ext == 'lsn') {
             type = 'lesson';
             } else{
                   debug.warn('User tried to load a non .lrn file: ' + ext);             
             }
         
           return type;
    
    },
     
  
		
	  verify: function (callback, onerror) {                                                     // Verify that the file is indeed a dot Learn file (by parsing the main json file)
	  	
	  	onerror = onerror || function () {};
	  	
	  
	  	   var filename = this.type + '.json';
	  	   
	  	   if(this.type === 'course' || this.type === 'lesson' ){
	  	   
	  	       
         loader.retrieveResource(filename, function (coursefile){
         	
         	
                  		var freader = new FileReader();
																  
						      freader.onload = function(){					  	                               // Make the verifying steps more rigorous
							                                                                               // Check for course ID and everything
							  				 var text = freader.result;
							  				 var json;
							  				 
							  				 try{												    
										    var json =  JSON.parse(text);    
										                                       							    
                                  }
                                  catch (e) {
                                  	
                                  	 debug.warn('An error occurred while parsing the JSON file- it may be corrupt');
                                  	 onerror(e);
                                  }                                 
                                 
                                 if(typeof json !== 'undefined'){
                                     callback(json);
                                  }    
  
							
										    
							   };
							   
							   freader.onerror = function () {
							   	
							   	debug.warn('Unable to read JSON file - either file doesnt exist, or is corrupt');
                          
                          onerror({message:event.target.error.code}); 							   
							   };
		
              
                     	freader.readAsText(coursefile);
         }, function (e) {
         	
         	
         	
                debug.warn('Problem occured while verifying the course file');    
                 onerror(e);
         });
         
        } else {
        	
         	debug.warn('User tried to upload an invalid filetype, or some problem finding the .json file');
             
            onerror({message: 'error-file-type'}); 	 
	  	  }       
			
		},
		
	  preview: {                                                                             //Preview object, to be used by the view
        name: 'Name',
        description: 'description',
        image: 'icons/gray-logo.png' 	  
	    
	  }, 
		
     setPreview: function (callback) {                                                      // Function to set the above object
      	
      	var preview = {
			      name: object.name,
			      description: object.description,
			      image: 'icons/gray-logo.png'
			  };
				
		    var fileHandler = this;
		    
		    loader.retrieveResource('meta/' + object.image, function (data) {
		    	
          	   preview.image = URL.createObjectURL(data);	     
					fileHandler.preview = preview;
					
					callback();

          });
      },
      
      
    lessons: [],

    previewLessons: function (callback) {                                   // On call of this function, if the file is a course, it generates preview objects for each lesson

 
    	var lessons = [];
      var handler = this;    	
    	
    	var i=0; 
    	
    	var next = function () {
    	              
            if (i<object.lessons.length){
            	
            	  var  previewLesson = lessonHandler.newLesson(object.lessons[i]);

						previewLesson.getPreview(function (preview) {
							
						     	lessons[i] = preview;
						     	i++;
						     	next();
						});            		
            		

		    	} else {
		    		handler.lessons = lessons;
				   callback();		 
		    	}
		    };
		    
		next();
  
    },
    
    
   getLesson: function (index) {                                                            // Retrieve, format and return lesson object for selected lesson, to be used in player.js
            
        if(index > -1){             
            lesson = lessonHandler.newLesson(object.lessons[index]); 	 	   
   	    } else {
   	      lesson = lessonHandler.newLesson(object);  	    
   	   }
   	
   	      lesson.course = object.id;
            lesson.index = index;
 	
   	      return  lesson;
   	},
   	
    getJSON: function () {
    
           return object;    
    },
    
    
    getBlob: function () {
       return loader.getBlob();
    },

    getFile: function (name, callback, onerror, onprogress) {
    

		  	         
		  	         loader.retrieveResource(name, callback, onerror, onprogress);
                      
    
    }
   
};
   	
	
})
.factory('lessonHandler', function (loader) {                                                // Singleton which determines the structure of the lesson objects used in file.js, player.js

	return {
		  
		    
			newLesson: function (inputLesson) {                                //return Lesson Object
                       
                  return {                                      
                  
                          object: inputLesson,
                         
                          length: inputLesson.items.length,
                         
                          getPreview: function (callback) {
      	
								      	var preview = {
											      name: this.object.name,
											      description: this.object.description,
											      image: 'icons/gray-logo.png'
											  };
												
										
										    loader.retrieveResource('meta/' + this.object.image, function (data) {
								          	   preview.image = URL.createObjectURL(data);	     
													callback(preview);
								          });
						      },
						      
						      
						      getItem: function (i) {
						      	
						      	  return this.object.items[i];
						      	
						      },
						      
						      getResource:function (name, callback, onprogress, onerror) { 
						      
						             loader.retrieveResource('res/'+name, callback, onprogress, onerror);
						      
						      }
                  
                  };				
				
			}
	
	 };

});





