describe('Unit:  dotLearn', function() {

    it('should should register a module called test, and register several events for that module', function () {

        expect(Object.keys(dotlearn.modules).length).toEqual(0);

        dotlearn.module('test', function(moduleManager){

             moduleManager.init({html: 'test.html'}, function (app) {});

            moduleManager.onStart({html: 'test.html'}, function (app) {});

            moduleManager.onLoad({html: 'test.html'}, function (app) {});

            moduleManager.onNext({html: 'test.html'}, function (app) {});

            moduleManager.onEnd({html: 'test.html'}, function (app) {});

            moduleManager.onEdit({html: 'test.html'}, function (app) {});

        });


        expect(Object.keys(dotlearn.modules).length).toEqual(1);

        var module = dotlearn.modules.test;

        expect(module).toBeDefined();

        expect(module.load.config.html).toEqual('test.html');

        expect(module.next.onNext).toBeDefined();

    });

});
