describe('Unit: LanguageController', function() {

    var lesson, file;

    beforeEach(module('app'));

    var ctrl;
    var scope;

    beforeEach(inject(function($controller, $rootScope){

        scope = $rootScope.$new();
        ctrl = $controller('UIController', {
            $scope: scope
        });

    }));


    it('should have a main controller', function () {

        expect(ctrl).toBeDefined();

    });


    it('should have a default language', function () {

        expect(scope.userLanguage).toEqual('');
        expect(scope.g).toBeUndefined();

    });


    it('should be able to set the language language', function (done) {

        scope.setLanguage('en');

        setTimeout(function () {
            expect(scope.g.lang).toEqual('English');
            done();
        }, 200);

    });





});
