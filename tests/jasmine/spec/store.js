describe('Unit: Store', function() {

    var mock, mock2, mock3;

    beforeEach(module('store'));

    beforeEach(function(done){

        mock = {
            id: '1',
            name: 'mock1',
            description: 'mock e-course',
            object: {}
        };

        mock2 = {
            id: '2',
            name: 'mock2',
            description: 'mock e-lesson',
            object: {}
        };

        mock3 = {
            id: '3',
            name: 'mock3',
            description: 'mock e-lesson',
            object: {}
        };

        inject(function (store) {
            store.init();
            setTimeout(done, 100);
        });

    });

    it('should have a store service', function () {

        inject(function (store) {
            expect(store).toBeDefined();
        });
    });

    it('should generate a list', function (done) {

        inject(function (store) {

                 store.list('course', function(files){
                     expect(files).toBeDefined();
                     expect(files.length).toEqual(0);
                     done();
                 });

        });

    });


    it('should should be able to store, retrieve and delete a  mock object', function (done) {

        inject(function (store) {

                store.set('course', mock, function () {

                     store.get('course', '1', function(result){

                                    expect(result.name).toEqual('mock1');

                                     store.remove('course', '1', function () {
                                                store.list('course', function(files){
                                                    expect(files).toBeDefined();
                                                    expect(files.length).toEqual(0);
                                                    done();
                                                });

                                     })

                     });

                });

        });

    });




it('should should be able to create several mock objects, and then delete all', function (done) {

    inject(function (store) {

        store.set('course', mock, function () {

            store.set('course', mock2, function () {

                store.set('course', mock3, function () {

                    store.list('course', function(files){
                        expect(files).toBeDefined();
                        expect(files.length).toEqual(3);

                           store.deleteAll('course', function () {
                               store.list('course', function(files) {
                                   expect(files).toBeDefined();
                                   expect(files.length).toEqual(0);
                                   done();
                               });

                           });
                    });

                });


            });


        });

    });

});

});