describe('Integration: Library', function() {

  var file;

    beforeEach(module('app'));

    beforeEach(function(done){

         file = a.lsn;
         file.name = 'a.lsn';

        inject(function (store, fileHandler) {
            store.init();
            fileHandler.open(file, done);
        });

    });

    it('should have a Library  service', function () {

        inject(function (library) {
            expect(library).toBeDefined();
        })
    });

    it('should return default (empty) lists', function (done) {

        inject(function (library) {
               library.refresh(function(a, b){

                   expect(a.length).toEqual(0);
                   expect(b.length).toEqual(0);

                   done();
               });

        })

    });


    it('should install and remove a lesson', function (done) {

        inject(function (library) {
            library.install(function(){

                library.refresh(function(a, b){

                    expect(a.length).toEqual(0);
                    expect(b.length).toEqual(1);

                    var lesson = b[0];

                    library.uninstall('lesson', lesson.id,  function () {

                         library.refresh(function (a,b) {

                             expect(a.length).toEqual(0);
                             expect(b.length).toEqual(0);
                             done();
                         });

                    });


                });


            });

        })

    });




});
