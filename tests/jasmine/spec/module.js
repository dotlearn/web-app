
describe('Unit: Module', function() {

    var lesson, file, frame;

    beforeEach(module('module'));

    beforeEach(function(){

        frame = document.createElement('iframe');

        frame.id = 'module-test';
        frame.style.display = 'none';
        document.body.appendChild(frame);

    });

    it('should be able to load a test module and run the on-load function', function (done) {

// Inject the final functions into the iframe's window
        frame.contentWindow.done = done;
       frame.contentWindow.expect = expect;

         dotlearn.module('test', function (mm) {
             mm.init({}, function (app) {});


             mm.onLoad({device: true}, function (app) {

                 expect(app.device).toBeDefined();
                 done();

             });
         });

         inject(function (moduleManager) {

         moduleManager.init('test', function () {

             moduleManager.load({

         name: 'test',
         data: {},
         loader: {},
         user: {},
         blocker: {}
         });


         });


         });

    });

});
