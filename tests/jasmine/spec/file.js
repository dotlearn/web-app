describe('Unit: File', function() {

    var file;

    beforeEach(module('app'));

    beforeEach(function(){
            zip.workerScriptsPath = "js/lib/zip/";
            file = a.lsn;
            file.name = 'a.lsn';
    });

    it('should have a file service', function () {

        inject(function (fileHandler) {
            expect(fileHandler).toBeDefined();
        });
    });

    it('should be able to open a file, extract contents', function (done) {

        inject(function (fileHandler) {

                fileHandler.open(file, function(){

                   var json = fileHandler.getJSON();

                    expect(json.name).toEqual("Hello World!");
                    done();

                });

        });

    });

        it('should be able to generate a Preview', function (done) {

            inject(function (fileHandler) {

                fileHandler.open(file, function(){

                       fileHandler.setPreview(function () {
                           expect(fileHandler.preview.name).toEqual("Hello World!");
                           done();
                       });

                });

            });

        });

            it('should be able to retrieve a known file', function (done) {

                inject(function (fileHandler) {

                    fileHandler.open(file, function(){

                        fileHandler.getFile('meta/onesies.jpg', function (data) {
                            expect(data).toBeDefined();
                            done();
                        });

                    });

                });

    });

});


