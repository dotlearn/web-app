describe('Unit: Loader', function() {

    var file;

    beforeEach(module('app'));

    beforeEach(function(){
        zip.workerScriptsPath = "js/lib/zip/";
        file = a.lsn;                    // a.lsn is a blob representing a real zip file called a.lsn, but loaded via file called a.js, to avoid dealing with user uploads or file get requests (karma seems to corrupt zip files hosted on the karma server - the get requests return junk)
    });

    it('should have a loader  service', function () {

        inject(function (loader) {
            expect(loader).toBeDefined();
        });
    });

    it('should be able to load lesson.json from a test lsn file', function (done) {

        inject(function (loader) {

               loader.load(file);
               loader.retrieveResource('lesson.json', function(data){

                   expect(data).toBeDefined();
                   done();
               }, function(){

                   done();
               });
        });

    });


});
