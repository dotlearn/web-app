describe('Unit: Intl', function() {

    var file;

    beforeEach(module('app'));


    beforeEach(function(){


    });

    it('should have an intl service', function () {

        inject(function (intl) {
            expect(intl).toBeDefined();
        });
    });

    it('should be able to retrieve the default launguage', function () {

        inject(function (intl) {

                     expect(intl.current()).toBe('en');

        });

    });


    it('should be able to set the language', function (done) {

        inject(function (intl) {

            intl.setLanguage('es', function () {

                  expect(intl.map).toBeDefined();
                expect(intl.map.lang).toEqual('Español');

                intl.setLanguage('en', function () {
                    expect(intl.map.lang).toEqual('English');
                    done();
                });

            });



        });

    });




});
