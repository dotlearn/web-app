describe('Unit:  User', function() {

     //var module = angular.module('app');

      beforeEach(module('user'));

    it('should have a user service', function () {

        inject(function (user) {
            expect(user).toBeDefined();
        });
    });


    it('should retrieve default data', function () {

        inject(function (userPreferences) {
            expect(userPreferences).toBeDefined();


            expect(userPreferences.get('language')).toBeDefined();

            expect(userPreferences.get('storeData')).toBeDefined();
        });
    });


    it('Should be able to set and get data', function () {

        inject(function (userPreferences) {

            userPreferences.set('language', 'fr');
            expect(userPreferences.get('language')).toEqual('fr');;

            userPreferences.set('storeData', false);
            expect(userPreferences.get('storeData')).toEqual(false);
        });
    });
});
