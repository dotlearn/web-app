describe('Unit: Player', function() {

    var lesson, file;

    beforeEach(module('app'));

    beforeEach(function(done){


         file = a.lsn;

        inject(function (playerHandler, fileHandler) {



            fileHandler.open(file, function () {

                playerHandler.open(fileHandler.getLesson(-1));
                done();
            });

        });


    });

    it('should have a playerHandler service', function () {

        inject(function (playerHandler) {
             expect(playerHandler).toBeDefined();
        })
    });

    it('should return the init status for the lessons', function () {

        inject(function (playerHandler) {
            expect(playerHandler.status.getLength()).toEqual(7);
            expect(playerHandler.status.getItem()).toEqual(0);
        })

    });

    it('should be able to properly set items', function () {

        inject(function (playerHandler) {

            playerHandler.setItem(2);

            expect(playerHandler.status.getItem()).toEqual(2);

            playerHandler.setItem(4);

            expect(playerHandler.status.getItem()).toEqual(4);
        })

    });

    it('should be able to block changing items', function () {

        inject(function (playerHandler) {

            playerHandler.setItem(2);

            expect(playerHandler.status.getItem()).toEqual(2);

            playerHandler.blocker.block();

            playerHandler.setItem(5);
            expect(playerHandler.status.getItem()).toEqual(2);
        });

    });



});
